.PHONY: build

build:
	go build -o key santa/cmd/santa
	go build -o check santa/cmd/claus