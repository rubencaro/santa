package main

import (
	"fmt"
	"os"
	"os/exec"
	"santa"
	"time"
)

func main() {

	level, exists := santa.Levels[os.Args[1]]
	if !exists {
		panic("Nonexisting level")
	}

	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()

	for {
		time.Sleep(1 * time.Second)
		fmt.Printf("\033[2K\r%v", santa.CalcCode(level.Length))
	}
}
