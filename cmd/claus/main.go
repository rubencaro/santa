package main

import (
	"bufio"
	"fmt"
	"os"
	"santa"
)

func main() {
	for {
		input := getInput()

		if applyLevel(input) {
			fmt.Println(level.Name)
			continue
		}

		if santa.CalcCode(level.Length) == input {
			victory()
		} else {
			defeat()
		}
	}
}

var level = santa.Levels["level1"]

func getInput() string {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("\nEnter Code: ")
	scanner.Scan()
	return scanner.Text()
}

func applyLevel(in string) bool {
	newLevel, ok := santa.Levels[in]
	if ok {
		level = newLevel
		return true
	}
	return false
}

func defeat() {
	for i := 0; i < 10000; i++ {
		fmt.Printf(`💩`)
	}
	fmt.Println("\n\n", failure)
	fmt.Println()
}

func victory() {
	for i := 0; i < 10000; i++ {
		fmt.Printf(`🙂`)
	}
	fmt.Println("\n\n", perfect)
	fmt.Println()
}

const perfect = "Perfect!!   ᕙ(`▽´)ᕗ"
const failure = "  ¯\\_(⊙_ʖ⊙)_/¯"
