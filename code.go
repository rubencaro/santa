package santa

import (
	"crypto/md5"
	"fmt"
	"time"
)

func CalcCode(lentgh int64) string {
	// get 30sec intervals as byte array
	t := time.Now().Unix() / 30
	bytes := []byte(fmt.Sprintf("%d", t))
	// get random string
	hash := md5.Sum(bytes)
	// print only given number of bytes
	code := fmt.Sprintf("%x", hash)[2 : 2+lentgh]
	return code
}

type level struct {
	Name   string
	Length int64
}

var Levels = map[string]level{
	"level1": {
		Name:   "LEVEL 1",
		Length: 6,
	},
	"level2": {
		Name:   "LEVEL 2",
		Length: 10,
	},
	"level3": {
		Name:   "LEVEL 3",
		Length: 20,
	},
}
